
from flask import Blueprint
'''
post/index 列表
post/info 详情
post/set 添加|编辑
post/ops 操作(删除|恢复)
'''

post = Blueprint('post', __name__)


@post.route('/')
def index():
    return [value for value in range(10,101,10)]


@post.route('/info')
def info():
    return "欢迎登录系统"

