
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello ,I Love Imooc'

@app.route('/my/<username>')
def my(username):
    return "my page:user %s" % (username)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
