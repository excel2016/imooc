"""路由注册"""
from flask_debugtoolbar import DebugToolbarExtension

from application import app
from contollers.index import index_page


toolbar = DebugToolbarExtension(app)

'''
拦截器处理 和 错误处理器
'''
from interceptors.auth import *
from interceptors.errorHandler import *


app.register_blueprint(index_page, url_prefix='/')
