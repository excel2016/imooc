

if __name__ == '__main__':
    student = {"Tom", "Jim", "Mary", "Tom", "Jack", "Rose"}
    print(student)
    if "Rose" in student:
        print("Rose在集合中")
    else:
        print("Rose不在集合中")

    a = set("abrasdfasdfs")
    b = set("sdafwerdfd")
    print(a)
    print(b)
    print(a - b) # 差集 取一个集合中另一集合没有的元素
    print(a | b) # 并集 取两集合全部的元素
    print(a & b) # 交集 取两集合公共的元素
    print(a ^ b) # 取集合 A 和 B 中不属于 A&B 的元素

