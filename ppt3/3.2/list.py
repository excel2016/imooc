

if __name__ == '__main__':
    ls = ["abcd", 786, 2.23, "imooc", 70.2]
    tinylist = [123, "imooc"]
    print(ls)
    print(ls[0])
    print(ls[1:3])
    print(ls[2:])
    print(tinylist * 2)
    print(ls + tinylist)

    tinylist.append("456")
    print(tinylist)