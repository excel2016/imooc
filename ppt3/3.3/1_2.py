
def use_logging(func_name):
    print(f"[debug] {func_name} is running")

def bar():
    use_logging("bar")
    print('i am bar')

def bar2():
    use_logging("bar2")
    print('i am bar2')

bar()
bar2()




