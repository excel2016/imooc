"""核心变量"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# app.config['SQLALCHEMY_DATABASE_URI'] = \
#     'mysql://root:root@127.0.0.1/mysql'
app.config['SQLALCHEMY_DATABASE_URI'] = \
    'mysql://root:123456@192.168.0.111/tmp_test'
db = SQLAlchemy(app)
