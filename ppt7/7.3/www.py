"""路由注册"""
from application import app

from indexController import index_page
from postController import post


app.register_blueprint(index_page, url_prefix='/imooc')
app.register_blueprint(post, url_prefix='/post')