
from flask import Flask

from indexController import index_page
from postController import post
from extensions import db

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = \
    'mysql://root:root@127.0.0.1/mysql'

db.init_app(app)

app.register_blueprint(index_page, url_prefix='/imooc')
app.register_blueprint(post, url_prefix='/post')


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)


