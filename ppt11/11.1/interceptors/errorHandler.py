from application import app

@app.errorhandler(404)
def error_404(error):
    return "404 Not Found"