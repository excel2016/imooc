#本地开发环境配置文件
from config.base_setting import *
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_RECORD_QUERIES = True
SQLALCHEMY_DATABASE_URI = "mysql://root:123456@192.168.0.107/movie_cat"
SECRET_KEY = 'imooc123456'

DOMAIN = {
    "www": "http://192.168.0.107:80",
}

#RELEASE_PATH = "/home/www/release_version"