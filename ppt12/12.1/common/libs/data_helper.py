
import datetime,time

def getCurrentTime(frm = "%Y-%m-%d %H:%M:%S"):
    dt = datetime.datetime.now()
    return dt.strftime(frm)


def convertDate(date_str):
    from_format = "%b. %d, %Y"
    to_format = "%Y-%m-%d %H:%M:%S"
    time_struct = time.strptime(date_str, from_format)
    times = time.strftime(to_format, time_struct)
    return times