
import sys,argparse,traceback,importlib


class RunJob():
    '''
    Job统一入口文件
    python manager.py runjob -m Test (jobs/tasks/Test.py)
    python manager.py runjob -m test/index (jobs/tasks/test/index.py)
    '''
    capture_all_args = True
    def run(self,*args,**kwargs):
        params_dict = kwargs
        if 'name' not in params_dict or not params_dict['name']:
            return self.tips()

        try:
            '''
            from jobs.tasks.test import JobTask
            '''
            module_name = params_dict['name'].replace('/','.')
            import_string = "jobs.tasks.%s"%(module_name)
            target = importlib.import_module(import_string)
            exit(target.JobTask().run(params_dict))
        except Exception as e:
            traceback.print_exc()
        return

    def tips(self):
        tip_msg = '''
        请正确的调度Job
        python manager.py runjob -m Test (jobs/tasks/Test.py)
        python manager.py runjob -m test/index (jobs/tasks/test/index.py)
        '''
        print(tip_msg)
        return




