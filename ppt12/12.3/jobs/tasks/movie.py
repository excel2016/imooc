
import requests,os,time,hashlib,json
import traceback

from application import app,db

from bs4 import BeautifulSoup
from common.libs.data_helper import getCurrentTime,convertDate
from urllib.parse import urlparse
from common.models.movie import Movie


class JobTask():
    '''
    python manager.py runjob -m movie -a list | parse

    crontab -e 编辑
    crontab -l 查看
    f1 f2 f3 f4 f5 program
    f1分钟 f2小时 f3一个月份的第几日 f4月份 f5一个星期中的第几天 program表示要执行的程序

    30 3 * * * { export ops_config=local && python3 /home/www/ppt10/10.3/manager.py runjob -m movie -a list ;}
    '''
    # 最新源码
    # http://www.jixuejima.cn/article/330.html
    # 人人影视
    # https://yyets.com/movies/page/2/
    def __init__(self):
        self.source = "yyets"
        self.url = {
            "num": 3,
            "url": "https://yyets.com/movies/page/#d#/",
            "path": "/tmp/%s/" % (self.source)
        }



    def run(self,params):
        '''
        第一步 首先 获取列表list html 回来，
              通过解析html 获取详情 的 url等信息，
              在根据详情url 获取详情html
        第二步 解析 详情的html
        '''
        act = params['act']
        self.date = getCurrentTime( frm = "%Y%m%d")
        if act == "list":
            self.getList()
            self.parseInfo()
        elif act == "parse":
            self.parseInfo()



    def getList(self):
        '''获取列表'''
        config = self.url
        path_root = config['path'] + self.date
        path_list = path_root + "/list"
        path_info = path_root + "/info"
        path_json = path_root + "/json"
        self.makeSuredirs(path_root)
        self.makeSuredirs(path_list)
        self.makeSuredirs(path_info)
        self.makeSuredirs(path_json)
        pages = range(1,config['num']+1)
        for idx in pages:
            tmp_path = path_list + "/" + str(idx)
            tmp_url = config['url'].replace("#d#",str(idx))
            print("get list : " + tmp_url)

            if os.path.exists(tmp_path):
                continue

            tmp_content = self.getHttpContent(tmp_url)
            self.saveContent(tmp_path,tmp_content)
            time.sleep(0.3)


        for idx in os.listdir(path_list):
            if idx.endswith(".swp"):
                continue
            tmp_content = self.getContent(path_list + "/" + str(idx))
            items_data = self.parseList(tmp_content)

            if not items_data:
                continue

            for item in items_data:
                tmp_json_path = path_json + "/" + item['hash']
                tmp_info_path = path_info + "/" + item['hash']
                if not os.path.exists(tmp_json_path):
                    self.saveContent(tmp_json_path,json.dumps(item,ensure_ascii=False))

                if not os.path.exists(tmp_info_path):
                    tmp_content = self.getHttpContent(item['url'])
                    self.saveContent(tmp_info_path,tmp_content)

                time.sleep(0.3)


    def parseList(self,content):
        data = []

        tmp_soup = BeautifulSoup(str(content), "html.parser")
        tmp_list = tmp_soup.select("div#contenedor div#archive-content article.movies")
        for tmp_item in tmp_list:
            tmp_name = tmp_item.select("div.data h3 a")[0].getText()
            tmp_href = tmp_item.select("div.data h3 a")[0]['href']

            ##获取封面图片
            tmp_target_cover = tmp_item.select("div.poster img")
            tmp_target_src = tmp_target_cover[0]['src']

            tmp_data = {
                "name" : tmp_name,
                "url" : tmp_href,
                "cover_pic": tmp_target_src,
                "hash" : hashlib.md5(tmp_href.encode("utf-8")).hexdigest(),
            }

            data.append(tmp_data)

        return data


    def parseInfo(self):
        '''解析详情信息'''
        config = self.url
        path_root = config['path'] + self.date
        path_info = path_root + "/info"
        path_json = path_root + "/json"
        for filename in os.listdir(path_info):
            tmp_json_path = path_json + "/" + filename
            tmp_info_path = path_info + "/" + filename
            tmp_data = json.loads(self.getContent(tmp_json_path))
            tmp_content = self.getContent(tmp_info_path)
            tmp_soup = BeautifulSoup(str(tmp_content), "html.parser")
            try:
                tmp_pub_date = convertDate(tmp_soup.select("div#single div.sheader div.extra span.date")[0].getText())
                tmp_desc = (tmp_soup.select("div#single div#info div.wp-content")[0]
                                    .getText().strip())
                tmp_desc = tmp_desc.replace(tmp_desc[tmp_desc.rfind('\n'):], '')
                tmp_classifies_div = tmp_soup.select("div#single div.sheader div.sgeneros a")
                tmp_classifies = []
                for tmp_classify in tmp_classifies_div:
                    tmp_classifies.append(tmp_classify.getText())
                tmp_actors_div = tmp_soup.select("div#single div#cast div.persons div.person div.data div.name a")[1:]
                tmp_actors = []
                for tmp_actor in tmp_actors_div:
                    tmp_actors.append(tmp_actor.getText())
                tmp_pics_div = tmp_soup.select("div#single div#info div#dt_galery img")
                tmp_pics = []
                for tmp_pic in tmp_pics_div:
                    tmp_pics.append(tmp_pic['src'].strip())

                tmp_link = ''
                try:
                    tmp_link = tmp_soup.select("div#single div.box_links div#videos table tr td a")[0]['href']
                except:
                    pass

                tmp_data['pub_date'] = tmp_pub_date
                tmp_data['desc'] = tmp_desc
                tmp_data['classify'] = "".join([tmp_classify + "|" for tmp_classify in tmp_classifies])[:-1]
                if tmp_actors:
                    tmp_data['actor'] = "".join([tmp_actor + "/" for tmp_actor in tmp_actors])[:-1]
                if tmp_link:
                    tmp_data['magnet_url'] = tmp_link
                tmp_data['source'] = self.source
                tmp_data['created_time'] = tmp_data['updated_time'] = getCurrentTime()

                if tmp_pics:
                    tmp_data['pics'] = json.dumps(tmp_pics)

                tmp_movie_info = Movie.query.filter_by( hash = tmp_data['hash'] ).first()
                if tmp_movie_info:
                    continue

                tmp_movie_info = Movie( **tmp_data )
                db.session.add(tmp_movie_info)
                db.session.commit()

            except Exception as e:
                tb = traceback.format_exc()

                title = tmp_soup.select("div#single div.sheader div.data h1")[0].getText()
                app.logger.error(title+": "+str(e))
                app.logger.error(tb)

        return True

    def getCompleteHref(self,tmp_href):
        '''获取完整的href'''
        if "https:" not in tmp_href and "//" in tmp_href:
            return "https:%s" % tmp_href

        if "http:" not in tmp_href:
            config = self.url
            url_info = urlparse(config['url'])
            url_domain = url_info[0] + "://" + url_info[1]
            return url_domain + tmp_href

        return tmp_href


    def getHttpContent(self,url):
        try:
            headers = {
                'Content-Type': 'text/plain; charset=UTF-8',
                "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36",
            }
            r = requests.get(url,headers=headers)
            if r.status_code != 200:
                return None
            return r.content
        except Exception:
            return None


    def saveContent(self,path,content):
        if content:
            with open(path, mode="w+",encoding="utf-8") as f:
                if type(content) != str:
                    content = content.decode("utf-8")

                f.write(content)
                f.flush()
                f.close()


    def getContent(self,path):
        if os.path.exists(path):
            with open(path, mode="r",encoding="utf-8") as f:
                return f.read()

        return ''


    def makeSuredirs(self,path):
        if not os.path.exists(path):
            os.makedirs(path)




