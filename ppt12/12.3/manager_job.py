"""启动入口"""
from application import app
from www import *
from jobs.launcher import RunJob
import click
from flask.cli import FlaskGroup


def create_app():
    return app

cli = FlaskGroup(create_app=create_app)

@cli.command("runjob")
@click.option("-m","--name",help="指定job名",required=True)
@click.option("-a", "--act",help="Job动作", required=False)
@click.option("-p", "--param",help="业务参数",
              default=(), required=False,multiple=True)
def handlerJob(**kwargs):
    RunJob().run(**kwargs)

if __name__ == '__main__':
    try:
        import sys
        sys.exit(cli())
    except Exception as e:
        import traceback
        traceback.print_exc()