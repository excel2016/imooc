"""路由注册"""
from flask_debugtoolbar import DebugToolbarExtension

from application import app


toolbar = DebugToolbarExtension(app)

'''
拦截器处理 和 错误处理器
'''
from interceptors.auth import *
from interceptors.errorHandler import *

'''
蓝图
'''
from contollers.index import index_page
from contollers.member import member_page
app.register_blueprint(index_page, url_prefix='/')
app.register_blueprint(member_page, url_prefix='/member')

'''
模板函数
'''
from common.libs.url_manager import UrlManager
app.add_template_global(UrlManager.buildStaticUrl,"buildStaticUrl")
app.add_template_global(UrlManager.buildUrl,"buildUrl")