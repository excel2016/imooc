"""启动入口"""
from application import app
from www import *


def main():
    app.run(host="0.0.0.0", port=5000)


if __name__ == '__main__':
    try:
        import sys
        sys.exit(main())
    except Exception as e:
        import traceback
        traceback.print_exc()