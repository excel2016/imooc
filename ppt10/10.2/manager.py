"""启动入口"""
from application import app,manager
from flask_script import Server,Command
from www import *
from jobs.launcher import RunJob

##web server
manager.add_command("runserver",
    Server(host="0.0.0.0",use_debugger=True,use_reloader=False))

# from jobs.movie import MovieJob
# manager.add_command("runjob",MovieJob)
manager.add_command("runjob",RunJob)


##create_table
@Command
def create_all():
    from application import db
    from common.models.user import User
    with app.app_context():
        db.create_all()

manager.add_command("create_all",create_all)


def main():
    manager.run()


if __name__ == '__main__':
    #app.run(,debug=True)
    try:
        import sys
        sys.exit(main())
    except Exception as e:
        import traceback
        traceback.print_exc()