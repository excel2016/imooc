# Python Flask练习



## 课程地址

[Python Flask入门与进阶 开发电影网站_课程 (imooc.com)](https://coding.imooc.com/learn/list/399.html)

课程练习代码



## 表结构

user

```sql
CREATE TABLE `user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nickname` VARCHAR(30) NOT NULL COMMENT '昵称',
  `login_name` VARCHAR(20) NOT NULL COMMENT '登录用户名',
  `login_pwd` VARCHAR(32) NOT NULL COMMENT '登录用户密码',
  `login_sait` VARCHAR(32) NOT NULL COMMENT '登录密码随机字符串',
  `status` TINYINT(3) NOT NULL DEFAULT '1' COMMENT '状态 0：无效 1：有效',
  `updated_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次更新时间',
  `created_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_login_name` (`login_name`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4
```

movie

```sql
CREATE TABLE `movie` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '电影名称',
  `classify` varchar(100) NOT NULL DEFAULT '' COMMENT '类别',
  `actor` varchar(500) NOT NULL DEFAULT '' COMMENT '主演',
  `cover_pic` varchar(300) NOT NULL DEFAULT '' COMMENT '封面图',
  `pics` varchar(1000) NOT NULL DEFAULT '' COMMENT '图片地址json',
  `url` varchar(300) NOT NULL DEFAULT '' COMMENT '电影详情地址',
  `desc` text NOT NULL COMMENT '电影描述',
  `magnet_url` varchar(5000) NOT NULL DEFAULT '' COMMENT '磁力下载地址',
  `hash` varchar(32) NOT NULL DEFAULT '' COMMENT '唯一值',
  `pub_date` datetime NOT NULL COMMENT '来源网址发布日期',
  `source` varchar(20) NOT NULL DEFAULT '' COMMENT '来源',
  `view_counter` int(11) NOT NULL DEFAULT '0' COMMENT '阅读数',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_hash` (`hash`),
  KEY `idx_pu_date` (`pub_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='影视数据表';
```



## 通过数据库生成Model

user

```shell
flask-sqlacodegen "mysql://root:123456@127.0.0.1/movie_cat"?charset=utf8mb4 --tables user --outfile "common/models/user.py" --flask
```

movie

```shell
flask-sqlacodegen "mysql://root:123456@127.0.0.1/movie_cat"?charset=utf8mb4 --tables movie --outfile "common/models/movie.py" --flask
```





## uwsgi命令启动

启动前需要设置

```shell
export ops_config=local
```

进入到项目路径

安装依赖

```shell
pip install -r requirement.txt
```

直接执行

```shell
uwsgi --http-socket :5000 --wsgi-file manager.py --callable app --processes 4 --threads 2
```

使用ini执行

```shell
# 启动
uwsgi --ini uwsgi.ini
# 重启
uwsgi --reload /tmp/logs/movie.pid
# 关闭
uwsgi --stop /tmp/logs/movie.pid
```

使用nginx

```shell
# 启动
sudo service nginx start
# 重启
sudo service nginx restart
# 关闭
sudo service nginx stop
```

nginx配置

cd /etc/nginx/conf.d

vim default.conf 

如果nginx + uwsgi 配置之后访问是502 等500 错误 。试试 setenforce 0

```
server {
    listen       80;
    server_name  localhost;   
    location / {
        try_files $uri @yourapplication;
    }
    location @yourapplication {
        include uwsgi_params;
        uwsgi_pass unix:/tmp/logs/movie.sock;
        uwsgi_read_timeout 1800;
        uwsgi_send_timeout 300;
    }

}

```

